<?php

namespace App\Http\Controllers;

use App\Models\categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    
    public function index()
    {
        $categorias = categoria::all();
        return response()->json($categorias);
    }

    public function store(Request $request)
    {
        $rules = ['categoria'=>'required|string|min:1|max:100'];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $categoria = new categoria($request->input());
        $categoria->save();
        return response()->json([
            'status' => true,
            'message' => 'Categoría creada exitosamente'
        ],200);
    }

    public function show(categoria $categoria)
    {
        return response()->json(['status' => true, 'data' => $categoria]);
    }

    public function update(Request $request, categoria $categoria)
    {
        $rules = ['categoria'=>'required|string|min:1|max:100'];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $categoria->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Categoría actualizada  exitosamente'
        ],200);
    }

    public function destroy(categoria $categoria)
    {
        $categoria->delete();
        return response()->json([
            'status' => true,
            'message' => 'Categoría eliminada exitosamente'
        ],200);
    }
}
