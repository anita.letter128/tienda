<?php

namespace App\Http\Controllers;

use App\Models\producto;
use App\Models\categoria;
use DB;
use Illuminate\Http\Request;

class productoController extends Controller
{
    public function index()
    {
        $productos = producto::select('productos.*','categorias.categoria as categoria')
        ->join('categorias','categorias.id','=','productos.categoria_id')
        ->paginate(10);
        return response()->json($productos);
    }

    public function store(Request $request)
    {
        $rules = [
            'nombre'=> 'required|string|min:1|max:50',
            'descripcion'=> 'required|string|min:1|max:100',
            'precio'=> 'required|regex:/^\d+(\.\d{10,2})?$/',
            'categoria_id'=> 'required|numeric'

        ];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()){ 
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $producto = new producto($request->input());
        $producto ->save();
        return response()->json([
            'status' => true,
            'message' => 'Producto creado satisfactoriamente'
        ],200);
    }

    public function show(producto $producto)
    {
        return response()->json(['status' => true, 'data' => $producto]);
    }

    public function update(Request $request,producto $producto)
    {
        $rules = [
            'nombre'=> 'required|string|min:1|max:50',
            'descripcion'=> 'required|string|min:1|max:100',
            'precio'=> 'required|regex:/^\d+(\.\d{10,2})?$/',
            'categoria_id'=> 'required|numeric'

        ];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()){ 
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $producto->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'producto actualizado satisfactoriamente'
        ],200);
    }

    public function destroy(producto $producto)
    {
       $producto->delete();
       return response()->json([
        'status' => true,
        'message' => 'producto eliminado satisfactoriamente'
    ],200);
    }

    public function productosbycategoria(){
        $productos = producto::select(DB::raw('count(productos.id) as count,
        categorias.categoria'))
        ->join ('categorias','categorias.id','=','productos.categoria_id')
        ->groupBy('categorias.categoria')->get();
        return response()->json($productos);
    }

    public function all(){
        $productos = producto::select(DB::raw('count(productos.id) as count,
        categorias.categoria'))
        ->join ('categorias','categorias.id','=','productos.categoria_id')
        ->get();
        return response()->json($productos);
    }

}
