<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\producto>
 */
class ProductoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [ //cambios nuevos aquí
           'nombre'=> $this->faker->nombre,
           'descripcion'=> $this->faker->descripcion,
           'precio'=> $this->faker->numberBetween(20,60),
           'categoria_id'=> $this->faker->numberBetween(1,6)
        ];
    }
}
